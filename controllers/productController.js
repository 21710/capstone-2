const Product = require("../models/Product")

module.exports.addProduct = (data) => {
	console.log(data.isAdmin)

	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return newProduct.save().then((newProduct, error) => {
			if(error){
				return error
			}

			return newProduct 
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve('User must be ADMIN to access this.')

	return message.then((value) => {
		return value
	})
}

module.exports.getAllProducts = () => {
	return Product.find().then(result => {
		return result
	})
}

module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result
	})
}

module.exports.updateProduct = (productId, newData) => {
	
	return Product.findByIdAndUpdate(productId, {
		name: newData.name,
		description: newData.description,
		price: newData.price 
	})
	.then((updateProduct, error) => {
		if(error){
			return false
		}

		return true
	})
}

module.exports.archiveProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {
		isActive: false
	})
	.then((archivedProduct, error) => {
		if(error){
			return false
		} 

		return true
	})
}

// New code

module.exports.unarchiveProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {
		isActive: true
	})
	.then((unarchivedProduct, error) => {
		if(error){
			return false
		} 

		return true
	})
}